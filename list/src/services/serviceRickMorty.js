 let requestRickMorty = {}
 
 requestRickMorty.get = async function({page, sortBy, sortDesc, name}) {
    const response = await require("axios").get(
        `https://rickandmortyapi.com/api/character?page=${page}&&name=${name || ""}`
    )
    let fields = [
        "id",
        "name",
        "status",
        "species",
        "type",
        "gender",
        "origin.name",
        "location.name",
    ];
    let alternativeText = "unknow"
    let characters = null
    let headers = []
    let rows = []

    fields.forEach((currentField, index) => {
        currentField = currentField.split(".")[0];
        headers[index] =
            index === 0
                ? {
                    text: currentField,
                    align: "start",
                    sortable: false,
                    value: currentField,
                }
                : { text: currentField, value: currentField };
    });
    characters = response.data.results
    characters.forEach((currentCharacter, index) => {
        rows[index] = {
            [fields[0]]: currentCharacter[fields[0]] || alternativeText,
            [fields[1]]: currentCharacter[fields[1]] || alternativeText,
            [fields[2]]: currentCharacter[fields[2]] || alternativeText,
            [fields[3]]: currentCharacter[fields[3]] || alternativeText,
            [fields[4]]: currentCharacter[fields[4]] || alternativeText,
            [fields[5]]: currentCharacter[fields[5]] || alternativeText,
            [fields[6].split(".")[0]]:
                currentCharacter[fields[6].split(".")[0]][fields[6].split(".")[1]],
            [fields[7].split(".")[0]]:
                currentCharacter[fields[6].split(".")[0]][fields[6].split(".")[1]],
        };
    });
    let total = response.data.info.count
    if (sortBy) {
        if (!sortDesc[0]) {
            console.log("descending")
            rows.sort((a, b) => {
                if (a[sortBy] > b[sortBy]) {
                    return 1;
                }
                if (a[sortBy] < b[sortBy]) {
                    return -1;
                }
                return 0;
            })

        }else{
            rows.sort((a, b) => {
                if (a[sortBy] < b[sortBy]) {
                    return 1;
                }
                if (a[sortBy] > b[sortBy]) {
                    return -1;
                }
                return 0;
            })
        }
    }
    headers.push({ text: 'Actions', value: 'actions', sortable: false })
    return { total, headers, rows }

}
requestRickMorty.getProfile = async function (id) {
    const response = await require("axios").get(
        `https://rickandmortyapi.com/api/character/${id}`
    )
    const {name, status, species, gender, image, origin} = response.data
    return {name, status, species, gender, image, origin:origin.name}

}

module.exports = requestRickMorty 