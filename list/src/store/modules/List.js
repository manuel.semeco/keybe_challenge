import serviceRickMorty from "@/services/serviceRickMorty";
const state = {
    editedItem : {
        id: "",
        name: "",
        status: "",
        species: "",
        type: "",
        genter: "",
        origin: "",
        location: "",
    },
    editedIndex: -1,
    users: [],
    dialog: {
        dialog:false,
        dialogDelete:false
    }
}
const mutations = {
    SET_EDITED_ITEM (state, editedItem) {
        state.editedItem = editedItem
    },
    SET_EDITED_INDEX (state, editedIndex) {
        state.editedIndex = editedIndex
    },
    SET_USERS (state, users) {
        state.users = users
    },
    PUSH_USER (state, user){
        console.log("push User")
        state.users.push(user)
    },
    MERGE_USERS (state, value){
        Object.assign(state.users[state.editedIndex],value)
    },
    SPLICE_USERS (state, value){
        state.users.splice(value, 1)
    }
}
const actions = {
    seteditedItem: (vuexContext, editedItem) => vuexContext.commit('SET_EDITED_ITEM', editedItem),
    seteditedIndex: (vuexContext, editedIndex) => vuexContext.commit('SET_EDITED_INDEX', editedIndex),
    modifyUsers (vuexContext, {operation, value}) {
        if(operation==="=") vuexContext.commit('SET_USERS',value)
        else if(operation=='push') vuexContext.commit('PUSH_USER',value)
        else if (operation=='merge') vuexContext.commit('MERGE_USER',value)
        else if (operation=='splice') vuexContext.commit('SPLICE_USERS',value)
    },
    async setUsers(vuexContext) {
        const response = await serviceRickMorty.get()
        console.log(vuexContext.state.users)
        vuexContext.commit('SET_USERS',response.rows)
        return response.headers
    }
}


export default {
    state,
    mutations,
    actions
  }
  