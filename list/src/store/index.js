import Vue from "vue";
import Vuex from "vuex";
// import List from "./modules/List"
import serviceRickMorty from "@/services/serviceRickMorty";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    editedItem: {
      id: "",
      name: "",
      status: "",
      species: "",
      type: "",
      genter: "",
      origin: "",
      location: "",
    },
    editedIndex: -1,
    users: [],
    dialog: {
      dialog: false,
      dialogDelete: false,
      dialogShow: false,
    },
    currentItem: {
      name: "",
      status: "",
      species: "",
      gender: "",
      origin: "",
      image:"",
    },
    options: {}
  },
  mutations: {
    SET_EDITED_ITEM(state, editedItem) {
      state.editedItem = editedItem
    },
    SET_EDITED_INDEX(state, editedIndex) {
      state.editedIndex = editedIndex
    },
    SET_USERS(state, users) {
      state.users = users
    },
    PUSH_USER(state, user) {
      state.users.push(user)
    },
    MERGE_USERS(state, value) {
      Object.assign(state.users[state.editedIndex], value)
    },
    SPLICE_USERS(state, value) {
      state.users.splice(value, 1)
    },
    SET_DIALOG(state, { key, value }) {
      state.dialog[key] = value
    },
    SET_CURRENT_ITEM(state, currentItem) {
      console.log(`this is the item at SET_CURRENT_ITEM ${JSON.stringify(currentItem)}`)
      Object.assign(state.currentItem, currentItem) 
    },
    SET_OPTIONS(state, options) {
      state.options = options
    }

  },
  actions: {
    SET_EDITED_ITEM: (vuexContext, editedItem) => vuexContext.commit('SET_EDITED_ITEM', editedItem),
    SET_EDITED_INDEX: (vuexContext, editedIndex) => vuexContext.commit('SET_EDITED_INDEX', editedIndex),
    MODIFY_USERS(vuexContext, { operation, value }) {
      if (operation === "=") vuexContext.commit('SET_USERS', value)
      else if (operation == 'push') vuexContext.commit('PUSH_USER', value)
      else if (operation == 'merge') vuexContext.commit('MERGE_USERS', value)
      else if (operation == 'splice') vuexContext.commit('SPLICE_USERS', value)
    },
    async SET_USERS(vuexContext, options) {
      const response = await serviceRickMorty.get(options)
      vuexContext.commit('SET_USERS', response.rows)
      return response
    },
    SET_DIALOG: (vuexContext, { key, value }) => vuexContext.commit('SET_DIALOG', { key, value }),
    async SET_CURRENT_ITEM (vuexContext, currentItem) {
      const response = await serviceRickMorty.getProfile(currentItem.id)
      vuexContext.commit('SET_CURRENT_ITEM', response)
    },
    SET_OPTIONS: (vuexContext, options) => vuexContext.commit('SET_OPTIONS',options)
  },
  modules: {
    // List
  }
});
